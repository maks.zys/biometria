package sample;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Controller {

    private double pixelX = -1,
            pixelY = -1;
    private WritableImage writableImage,
            writableImageOriginal,
            tmpWritableImage;

    @FXML
    private Button btn_exit,
            btn_loadPicture,
            btn_changeCollor,
            btn_save,
            btn_openHistogramsWindow,
            btn_openHistogramsWindow1,
            btn_openHistogramsWindow11,
            btn_brightnessAccept,
            btn_brightnessCancel,
            btn_executeBinarization,
            btn_acceptBinarization,
            btn_undoBinarization,
            btn_executeOtsu,
            btn_acceptOtsu,
            btn_undoOtsu,
            btn_executeNiblack,
            btn_lowPass,
            btn_PrewittFilter,
            btn_horizontalSobelFilter,
            btn_LaplaceFilter,
            btn_CornersDetectionFilter,
            btn_undoFilters,
            btn_medianFilter,
            btn_kuwaharaFilter,
            btn_startKMM,
            btn_undoFingerPrints,
            btn_setFingerPrints,
            btn_minutiae,
            btn_fingerprintAlg;

    @FXML
    private ColorPicker colPick_RGBColor;

    @FXML
    private ImageView iv_image,
            iv_imageOriginal;

    @FXML
    private Slider slider_size,
                    slider_from,
                    slider_to,
                    slider_brightness;

    @FXML
    private Label label_rgbRedValue,
            label_rgbGreenValue,
            label_rgbBlueValue;

    @FXML
    private TabPane pane_options;

    @FXML
    private LineChart defaultChart;

    @FXML
    private TextField ta_binarizationThreshold,
            ta_niblackFrameSize,
            ta_niblackCoefficent;

    @FXML
    private ComboBox combobox_filterWinSize;

    public void initialize(){
        onLoadEvents();
        buttonClickedEvents();
        sliderValueChangedEvents();
        mouseClickEvents();
        keyClickedEvents();
    }



    //Window events
    public void onLoadEvents(){
        pane_options.setDisable(true);
        prepareSliders();
        prepareCombobox();
    }

    public void buttonClickedEvents(){
        btn_exit.setOnAction(event -> {
            System.exit(0);
        });

        btn_loadPicture.setOnAction(event -> {
            try {
                loadImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btn_changeCollor.setOnAction(event -> changePixelRGBColor());

        btn_save.setOnAction(event -> saveImage());

        btn_openHistogramsWindow.setOnAction(event -> {
            try {
                openHistogramWindow(1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btn_openHistogramsWindow1.setOnAction(event -> {
            try {
                openHistogramWindow(2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btn_openHistogramsWindow11.setOnAction(event -> {
            try {
                openHistogramWindow(3);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btn_brightnessAccept.setOnAction( event -> {
            writableImage = tmpWritableImage;
            setImage(writableImage);
            slider_brightness.setValue(0.0);
        });

        btn_brightnessCancel.setOnAction( event -> {
            setImage(writableImage);
            tmpWritableImage = null;
            slider_brightness.setValue(0.0);
        });

        btn_executeBinarization.setOnAction( event -> {
            executeBinarization();
        });
        btn_acceptBinarization.setOnAction( event -> {
            changeImage();
            ta_binarizationThreshold.setText("");
        });
        btn_undoBinarization.setOnAction( event -> {
            undoChanes();
            ta_binarizationThreshold.setText("");
        });

        btn_executeOtsu.setOnAction( event -> {executeOtsu();});
        btn_acceptOtsu.setOnAction(event -> {
            changeImage();
        });
        btn_undoOtsu.setOnAction(event -> {
            undoChanes();
        });

        btn_executeNiblack.setOnAction( event -> {
            executeNiblack();
        });

        btn_lowPass.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.lowPassFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_PrewittFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.prewittFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_horizontalSobelFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.horizontalSobelFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_LaplaceFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.laplaceFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_CornersDetectionFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.cornersDetectionFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_undoFilters.setOnAction(event -> {
            undoChanes();
        });
        btn_medianFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.medianFilter(writableImage);
            setImage(tmpWritableImage);
        });
        btn_kuwaharaFilter.setOnAction(event -> {
            Filter filter = new Filter(combobox_filterWinSize);
            tmpWritableImage = filter.kuwaharaFilter(writableImage);
            setImage(tmpWritableImage);
            System.out.println("DONE");
        });

        btn_startKMM.setOnAction(event -> {
            KMMThinning kmmThinning = new KMMThinning((int)writableImage.getWidth(), (int)writableImage.getHeight());
            tmpWritableImage = kmmThinning.makeKMMThinning(writableImage);
            setImage(tmpWritableImage);
            System.out.println("KMM operation done");
        });
        btn_undoFingerPrints.setOnAction(event -> undoChanes());
        btn_setFingerPrints.setOnAction(event -> {
            changeImage();
            System.out.println("Picture changed");
        });

        btn_minutiae.setOnAction(event -> {
            Minutiae minutiae = new Minutiae((int)writableImage.getWidth(), (int)writableImage.getHeight());
            tmpWritableImage = minutiae.startMinutiaeSearch(writableImage);
            setImage(tmpWritableImage);
            System.out.println("Minutiae operation done");
        });
        btn_fingerprintAlg.setOnAction(event -> {
            KMMThinning kmmThinning = new KMMThinning((int)writableImage.getWidth(), (int)writableImage.getHeight());
            tmpWritableImage = kmmThinning.makeKMMThinning(writableImage);
            changeImage();
            Minutiae minutiae = new Minutiae((int)writableImage.getWidth(), (int)writableImage.getHeight());
            tmpWritableImage = minutiae.startMinutiaeSearch(writableImage);
            setImage(tmpWritableImage);
            System.out.println("Finger algorithms done");
        });
    }

    public void sliderValueChangedEvents(){
        slider_size.valueProperty().addListener(e -> resize());
        slider_brightness.valueProperty().addListener( event -> {
            changeBrightness(slider_brightness.getValue());
        });
    }

    public void mouseClickEvents(){
        iv_image.setOnMouseClicked(e -> {
            MouseButton mouseButton = e.getButton();
            if (mouseButton == MouseButton.PRIMARY)
                changePixelRGBColor();
            else if (mouseButton == MouseButton.SECONDARY)
                checkPixelRGBbyClick(e.getX(), e.getY());
        });

    }

    public void keyClickedEvents(){}



    //default options
    public final void prepareSliders(){
        slider_size.setMin(1);
        slider_size.setMax(15);
        slider_size.setValue(1);
        slider_size.setShowTickLabels(true);
        slider_size.setShowTickMarks(true);
        slider_size.setMajorTickUnit(1);
        slider_size.setBlockIncrement(1);
        slider_size.setShowTickLabels(true);

        slider_from.setMin(0);
        slider_from.setMax(255);
        slider_from.setValue(1);
        slider_from.setShowTickLabels(true);

        slider_to.setMin(0);
        slider_to.setMax(255);
        slider_to.setValue(1);
        slider_to.setShowTickLabels(true);
    }

    public final void prepareCombobox(){
        combobox_filterWinSize.getItems().addAll("3x3", "5x5");
        combobox_filterWinSize.setValue("3x3");
    }

    public final void loadImage() throws IOException {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter exFilter = new FileChooser.ExtensionFilter("All Handled Files",
                "*.jpg", "*.bmp", "*.gif", "*.tiff", "*tif", "*.png");
        fileChooser.getExtensionFilters().add(exFilter);
        File selectedPicture = fileChooser.showOpenDialog(null);
        if (selectedPicture != null){
            Image image = SwingFXUtils.toFXImage(ImageIO.read(selectedPicture), null);
            writableImage = new WritableImage(image.getPixelReader(), (int)image.getWidth(), (int)image.getHeight());
            writableImageOriginal = new WritableImage(image.getPixelReader(), (int)image.getWidth(), (int)image.getHeight());

            iv_imageOriginal.setImage(writableImageOriginal);
            iv_imageOriginal.setFitHeight(image.getHeight());
            iv_imageOriginal.setFitWidth(image.getWidth());

            iv_image.setImage(writableImage);
            iv_image.setFitWidth(image.getWidth());
            iv_image.setFitHeight(image.getHeight());
            pane_options.setDisable(false);
        }
    }

    public void saveImage(){

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(0, new FileChooser.ExtensionFilter("PNG", "*.png"));
        fileChooser.getExtensionFilters().add(1, new FileChooser.ExtensionFilter("JPG", "*.jpg"));
        fileChooser.getExtensionFilters().add(2, new FileChooser.ExtensionFilter("GIF", "*.gif"));
        fileChooser.getExtensionFilters().add(3, new FileChooser.ExtensionFilter("TIF", "*.tif"));

        File file = fileChooser.showSaveDialog(null);
        if (file == null)
            return;
        String ext = fileChooser.getSelectedExtensionFilter().getDescription().toLowerCase();
        try{
            BufferedImage bImage = SwingFXUtils.fromFXImage(writableImage, null);
            ImageIO.write(bImage, ext, file);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public final void resize(){
        iv_image.setFitHeight(iv_image.getImage().getHeight() * slider_size.getValue());
        iv_image.setFitWidth(iv_image.getImage().getWidth() * slider_size.getValue());

        iv_imageOriginal.setFitHeight(iv_image.getImage().getHeight() * slider_size.getValue());
        iv_imageOriginal.setFitWidth(iv_image.getImage().getWidth() * slider_size.getValue());

    }

    public final void checkPixelRGBbyClick(double x, double y){

        PixelReader pixelReader = iv_image.getImage().getPixelReader();
        double scale = slider_size.getValue();
        Color pixelColor;
        if (scale == 0)
            pixelColor = pixelReader.getColor((int)x, (int)y);
        else
            pixelColor = pixelReader.getColor((int)(x/scale), (int)(y/scale));

        label_rgbRedValue.setText("Red: " + Math.round(pixelColor.getRed() * 255));
        label_rgbGreenValue.setText("Green: " + Math.round(pixelColor.getGreen() *255));
        label_rgbBlueValue.setText("Blue: " + Math.round(pixelColor.getBlue()) *255);

        pixelX = x;
        pixelY = y;
    }

    public final void changePixelRGBColor(){
        if (pixelX != -1 && pixelY != -1){
            double scale = slider_size.getValue();
            if (scale == 0)
                writableImage.getPixelWriter().setColor((int)pixelX, (int)pixelY, colPick_RGBColor.getValue());
            else
                writableImage.getPixelWriter().setColor((int)(pixelX/scale), (int)(pixelY/scale), colPick_RGBColor.getValue());
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("First choose pixel to change");
            alert.showAndWait();
        }
    }



    //Open histogram window
    public void openHistogramWindow(int option) throws IOException {
        Histogram histogram = new Histogram(writableImage, this);
        histogram.openHistogramWindow(option);
    }

    public int[] getSliderValues(){
        return new int[]{ (int) slider_from.getValue(), (int) slider_to.getValue()};
    }

    public void setImage(WritableImage image){
        iv_image.setImage(image);
    }

    private double cutToFit(double value, double min, double max) {
        return Math.min(max, Math.max(min, value));
    }

    public void changeBrightness(double factor){
//        System.out.println("factor = " + factor);
        if (tmpWritableImage == null){
            tmpWritableImage = new WritableImage((int) writableImage.getWidth(), (int)writableImage.getHeight());
        }
        PixelWriter tmpPixelWriter = tmpWritableImage.getPixelWriter();

        if (factor <= 0)
            factor += 1.0;
        else
            factor = Math.pow(factor + 1, 2);

        PixelReader pixelReader = writableImage.getPixelReader();

        for (int y = 0; y < writableImage.getHeight(); y++) {
            for (int x = 0; x < writableImage.getWidth(); x++) {
                Color color = pixelReader.getColor(x, y);
                double newAndrzej = factor;
                Color newColor = new Color(
                        cutToFit(color.getRed()*newAndrzej, 0.0, 1.0),
                        cutToFit(color.getGreen()*newAndrzej, 0.0, 1.0),
                        cutToFit(color.getBlue()*newAndrzej, 0.0, 1.0),
                        color.getOpacity()
                );
                tmpPixelWriter.setColor(x, y, newColor);
            }
        }

        if (iv_image.getImage() != tmpWritableImage)
            setImage(tmpWritableImage);
    }



//    //Binarization, Otsu,
    public void executeBinarization(){
        int threshold = -1;
        try {
            threshold = Integer.parseInt(ta_binarizationThreshold.getText());
        }
        catch (Exception e){
            System.out.println("Zły format danych. Problem przy zamianie na integer." + e);
        }

        Exercise3 exercise3 = new Exercise3();
        tmpWritableImage = exercise3.binarization(writableImage, threshold);
        setImage(tmpWritableImage);
    }

    public void executeOtsu(){
        Exercise3 exercise3 = new Exercise3();
        setImage(exercise3.otsu(writableImage));
    }

    public void changeImage(){
        writableImage = tmpWritableImage;
        tmpWritableImage = null;
    }

    public void undoChanes(){
        setImage(writableImage);
        tmpWritableImage = null;
    }

    public void executeNiblack(){
        Exercise3 exercise3 = new Exercise3();
        tmpWritableImage = exercise3.niblack(writableImage,
                Integer.parseInt(ta_niblackFrameSize.getText()),
                Double.parseDouble(ta_niblackCoefficent.getText()));
        setImage(tmpWritableImage);
        System.out.println("done");
    }



//  Filters

}
