package sample;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KMMThinning {

    final static List<Integer> numbersToDelete = new ArrayList<>(
            Arrays.asList(3, 5, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31, 48, 52, 53, 54, 55,
            56, 60, 61, 62, 63, 65, 67, 69, 71, 77, 79, 80, 81, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 97,
            99, 101, 103, 109, 111, 112, 113, 115, 116, 117, 118, 119, 120, 121, 123, 124, 125, 126, 127, 131, 133,
            135, 141, 143, 149, 151, 157, 159, 181, 183, 189, 191, 192, 193, 195, 197, 199, 205, 207, 208, 209, 211,
            212, 213, 214, 215, 216, 217, 219, 220, 221, 222, 223, 224, 225, 227, 229, 231, 237, 239, 240, 241, 243,
            244, 245, 246, 247, 248, 249, 251, 252, 253, 254, 255));

    final static List<Integer> valuesPoints = new ArrayList<>(
            Arrays.asList(128, 1, 2, 64, 0, 4, 32, 16, 8));

    final static List<Integer> neighborhoodCheck = new ArrayList<>(
            Arrays.asList(3, 12, 48, 192, 6, 24, 96, 129,
            14, 56, 131, 224, 7, 28, 112, 193,
            195, 135, 15, 30, 60, 120, 240, 225));

    private int[][] picArr;
    private int width, height;
    private boolean again;

    public KMMThinning(int width, int height){
        this.width = width;
        this.height = height;
        this.again = true;
        picArr = new int[width][height];
    }

    private void convertToBinaryArr(PixelReader reader){
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                Color color = reader.getColor(x, y);
                if(color.equals(Color.BLACK))
                    picArr[x][y] = 1;
                else
                    picArr[x][y] = 0;
            }
        }
    }

    private void findPointsStickedToBackground(){
        for (int y = 1; y < height-1; y++) {
            for (int x = 1; x < width-1; x++) {
                if ((picArr[x][y+1] == 0 || picArr[x+1][y] == 0 || picArr[x][y-1] == 0 || picArr[x-1][y] == 0) && picArr[x][y] != 0)
                    picArr[x][y] = 2;
                else if ((picArr[x+1][y+1] == 0 || picArr[x+1][y-1] == 0 || picArr[x-1][y-1] == 0 || picArr[x-1][y+1] == 0) && picArr[x][y] != 0)
                    picArr[x][y] = 3;
            }
        }
    }

//    private void findCornersPointsStickedToBackGround(){
//        for (int y = 1; y < height-1; y++) {
//            for (int x = 1; x < width-1; x++) {
//                if ((picArr[x+1][y+1] == 0 || picArr[x+1][y-1] == 0 || picArr[x-1][y-1] == 0 || picArr[x-1][y+1] == 0) && picArr[x][y] != 0)
//                    picArr[x][y] = 3;
//            }
//        }
//    }

//    private void markCorners(){
//        for (int y = 1; y < height-1; y++) {
//            for (int x = 1; x < width-1; x++) {
//                if(checkIfItsCorner(x-1, y-1) == true)
//                    picArr[x][y] = 4;
//            }
//        }
//        deleteAll4Pixels();
//    }
//
//    private boolean checkIfItsCorner(int startX, int startY){
//        List<Integer> pixelsSquareList = new ArrayList();
//        int counter = 0;
//        for (int y = 0; y < 3; y++) {
//            for (int x = 0; x < 3; x++) {
//                pixelsSquareList.add(picArr[startX+x][startY+y]);
//            }
//        }
//
//        pixelsSquareList.remove(4);
//
//        for (int val : pixelsSquareList){
//            if (val == 0)
//                ++counter;
//        }
//
//        if (counter > 3 && counter < 7)
//            return true;
//
//        return false;
//    }
//
//    private void deleteAll4Pixels(){
//        for (int y = 0; y < picArr[0].length; y++) {
//            for (int x = 0; x < picArr.length; x++) {
//                if (picArr[x][y] == 4){
//                    picArr[x][y] = 0;
//                    again = true;
//                }
//            }
//        }
//    }
//
//    private void deleteOverPlusPixels(){
//        List<Integer> codesForOutlinePixels = new ArrayList<Integer>(Arrays.asList(3, 6, 7, 12, 4, 15, 24, 28, 30, 48,
//                56, 60, 96, 112, 120, 129, 131, 135, 192, 193, 195, 224, 225));
//
//        for (int y = 1; y < picArr[0].length -1; y++) {
//            for (int x = 1; x < picArr.length -1; x++) {
//                picArr[x][y] = calculatePixel(x-1, y-1);
//            }
//        }
//    }
//
//    private int calculatePixel(int startX, int startY){
//        int counter = 0, sum = 0;
//
//        for (int y = 0; y < 3; y++) {
//            for (int x = 0; x < 3; x++) {
//                sum += (picArr[startX+x][startY+y] !=  0) ? valuesPoints.get(counter) : 0;
//                ++counter;
//            }
//        }
//
//        if (numbersToDelete.contains(sum))
//            return 0;
//
//        return picArr[startX+1][startY+1];
//    }

    private void findNeighborhoods(){
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                if (picArr[x][y] == 2)
                    calculatePixelNeighborhoods(x-1, y-1);
            }
        }
    }

    private void calculatePixelNeighborhoods(int startX, int startY){
        int counter = 0, sum = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                sum += (picArr[startX+x][startY+y] != 0) ? valuesPoints.get(counter) : 0;
                ++counter;
            }
        }

        if (neighborhoodCheck.contains(sum))
            picArr[startX+1][startY+1] = 4;
    }

    private void deleteAll4(){
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (picArr[x][y] == 4){
                    picArr[x][y] = 0;
                    again = true;
                }
            }
        }
    }

    private void deleteNotNeedlessPixels(int pixel){
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                if (picArr[x][y] == pixel)
                    checkIsPixelNeedless(x-1, y-1);
            }
        }
    }

    private void checkIsPixelNeedless(int startX, int startY){
        int counter = 0, sum = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                sum += (picArr[startX+x][startY+y] != 0) ? valuesPoints.get(counter) : 0;
                ++counter;
            }
        }

        if (numbersToDelete.contains(sum)){
            picArr[startX+1][startY+1] = 0;
            again = true;
        }
    }



    public WritableImage makeKMMThinning(WritableImage image){
        PixelReader reader = image.getPixelReader();
        WritableImage thinnedImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelWriter writer = thinnedImage.getPixelWriter();

        Exercise3 exercise3 = new Exercise3();
        WritableImage afterOtsuImage = exercise3.otsu(image);
        PixelReader afterOtsuReader = afterOtsuImage.getPixelReader();

        //Zamiana czarnych na 1
        convertToBinaryArr(afterOtsuReader);
        do {
            again = false;
            //Stykanie sie z tłem na 2 i z rogami na 3
            findPointsStickedToBackground();

            //Wygłądzanie szukając i usuwając 4
            findNeighborhoods();
            deleteAll4();

            //Usuwanie nadmiarowych elementów
            deleteNotNeedlessPixels(2);
            deleteNotNeedlessPixels(3);

        }while (again);

        for (int y = 0; y < picArr[0].length; y++) {
            for (int x = 0; x < picArr.length; x++) {
                writer.setColor(x, y, (picArr[x][y] == 0) ? Color.WHITE : Color.BLACK);
            }
        }

        return thinnedImage;
    }

}
