package sample;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;


public class Exercise3 {

    private WritableImage newImage;

    public WritableImage binarization(WritableImage image, int threshold){

        PixelReader pixelReader = image.getPixelReader();
        newImage = new WritableImage(pixelReader, 0, 0, (int)image.getWidth(), (int)image.getHeight());

        PixelWriter pixelWriter = newImage.getPixelWriter();

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                Color color = pixelReader.getColor(x, y);

                int sumRGB = (int)(((color.getRed() + color.getBlue() + color.getGreen())/3)*256);
                if ((sumRGB <= threshold)) {
                    pixelWriter.setColor(x, y, Color.BLACK);
                } else {
                    pixelWriter.setColor(x, y, Color.WHITE);
                }
            }
        }
        return newImage;
    }

    private int[] otsuHistogram(PixelReader pixelReader, int length, int heigth){

        int[] grayHistogram = new int[256];

        for (int y = 0; y < heigth; y++) {
            for (int x = 0; x < length; x++) {
                Color color = pixelReader.getColor(x, y);
                grayHistogram[(int)(((color.getRed() + color.getBlue() + color.getGreen())/3)*255)]++;
            }
        }
        return grayHistogram;
    }

    private int sumAllHistogramVallues(int[] histogramValues){
        int sum = 0;

        for (int i = 0; i < 256; i++) {
            sum += i * histogramValues[i];
        }

        return sum;
    }

    private int otsuAlgorithm(WritableImage image){
        PixelReader pixelReader = image.getPixelReader();

        int[] histogramValues = otsuHistogram(pixelReader,
                (int)image.getWidth(),
                (int)image.getHeight());
        int histogramSum = sumAllHistogramVallues(histogramValues);

        int imageSize = (int)image.getHeight() * (int)image.getWidth();

        float sumBack = 0,
                varMax = 0;
        int weightBack = 0,
                weightFront = 0,
                threshold = 0;

        for (int i = 0; i < 256; i++) {
            weightBack += histogramValues[i];
            if (weightBack == 0)
                continue;

            weightFront = imageSize - weightBack;
            if (weightFront == 0)
                break;

            sumBack += (float)(i * histogramValues[i]);

            float meanBack = sumBack/weightBack;
            float meanFront = (histogramSum - sumBack)/weightFront;

            float varBetween = (float)weightBack * (float)weightFront * (float)Math.pow((meanBack - meanFront), 2);

            if (varBetween > varMax){
                varMax = varBetween;
                threshold = i;
            }
        }
        System.out.println(threshold);
        return threshold;
    }

    public WritableImage otsu(WritableImage image){
        return binarization(image, otsuAlgorithm(image));
    }

    private int checkDistances(int axis, int halfWindow, int maxSize){
        int start = axis - halfWindow;
        if (start < 0){
            return 0;
        }
        else if (start + (halfWindow*2+1) >= maxSize){
            return maxSize - (halfWindow*2+1);
        }
        return start;
    }

    public WritableImage niblack(WritableImage image, int frameSize, double coefficent){

        int imageHeight = (int)image.getHeight(),
                imageWidth = (int)image.getWidth(),
                startX,
                startY,
                halfFrame = (frameSize - 1)/2,
                framePixelsAmount = frameSize * frameSize,
                threshold;
        Color color;
        PixelReader pixelReader = image.getPixelReader();
        WritableImage newNiblackImage = new WritableImage(imageWidth, imageHeight);
        PixelWriter pixelWriter = newNiblackImage.getPixelWriter();

        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                double mean = 0.0,
                        stdevStep1 = 0.0,
                        stdev = 0.0,
                        greyScale,
                        frameAvg,
                        outputValue;
                ArrayList<Double> allValues = new ArrayList<Double>();

                startX = checkDistances(x, halfFrame, imageWidth);
                startY = checkDistances(y, halfFrame, imageHeight);

                for (int frameY = 0; frameY < frameSize; frameY++) {
                    for (int frameX = 0; frameX < frameSize; frameX++) {
                        color = pixelReader.getColor(startX + frameX, startY + frameY);
                        frameAvg = ((color.getRed()+color.getGreen()+color.getBlue())*255.0)/3.0;
                        allValues.add(frameAvg);
                        mean += (frameAvg/(double)framePixelsAmount);
                    }
                }

                for (double vallue : allValues){
                    stdevStep1 += Math.pow(vallue - mean, 2);
                }

                stdev = Math.sqrt(stdevStep1 / (double)framePixelsAmount);

                threshold = (int)(mean + coefficent * stdev);

                color = pixelReader.getColor(x, y);
                greyScale = ((color.getRed() + color.getGreen() + color.getBlue())*255.0)/3.0;

                if (greyScale < threshold){
                    pixelWriter.setColor(x, y, Color.BLACK);
                }
                else {
                    pixelWriter.setColor(x, y, Color.WHITE);
                }
            }
        }
        return newNiblackImage;
    }

//    public WritableImage niblack(WritableImage image, int windowSize, double coefficent){
//        PixelReader pixelReader = image.getPixelReader();
//        WritableImage niblackImage = new WritableImage(pixelReader, (int)image.getWidth(), (int)image.getHeight());
//        PixelWriter pixelWriter = niblackImage.getPixelWriter();
//        int pixelXStart, pixelXStop, pixelYStart, pixelYStop,
//                halfWindow = (windowSize - 1)/2,
//                imageHeight = (int)image.getHeight(),
//                imageWidth = (int)image.getWidth(),
//                allPixels = (int)Math.pow(windowSize, 2);
//        int[] actualAxis;
//        double mean = 0,
//                k = 0,
//                avg = 0,
//                pictureGreyScale;
//
//
//        for (int y = 0; y < imageHeight; y++) {
//            for (int x = 0; x < imageWidth; x++) {
//                actualAxis = checkDistances(x, halfWindow, imageWidth);
//                pixelXStart = actualAxis[0];
//                pixelXStop = actualAxis[1];
//                actualAxis = checkDistances(y, halfWindow, imageHeight);
//                pixelYStart = actualAxis[0];
//                pixelYStop = actualAxis[1];
//
//                ArrayList<Double> values = new ArrayList<Double>();
//                for (int windowY = 0; windowY < windowSize; windowY++) {
//                    for (int widnowX = 0; widnowX < windowSize; widnowX++) {
//                        Color color = pixelReader.getColor(pixelXStart + widnowX, pixelYStart + y);
//                        double greyScale = ((color.getRed()+color.getBlue()+color.getGreen())*255)/3;
//                        values.add(greyScale);
//                        avg +=greyScale/Math.pow(windowSize, 2);
//                        mean += greyScale;
//                    }
//                }
//                mean /= (double)allPixels;
//                for (double v : values){
//                    k += Math.pow(v - mean, 2);
//                }
//
//                double stdev = Math.sqrt(k / allPixels);
//                int localThreshold = (int)(avg + (stdev * coefficent));
//
//                Color createdColor = pixelReader.getColor(x, y);
//                 pictureGreyScale = ((createdColor.getRed() +
//                        createdColor.getGreen() +
//                        createdColor.getBlue())*255/3) < localThreshold ? 0.0 : 1.0;
//
//                 pixelWriter.setColor(x, y, new Color(pictureGreyScale, pictureGreyScale, pictureGreyScale, 1.0));
//            }
//        }
//
//        return niblackImage;
//    }

}
