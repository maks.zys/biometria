package sample;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Minutiae {

    private int width, height;

    public Minutiae(int width, int height){
        this.width = width;
        this.height = height;
    }

    private int[][] convertToBinaryArr(PixelReader reader){
        int[][] picArr = new int[width][height];
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                Color color = reader.getColor(x, y);
                if(color.equals(Color.WHITE))
                    picArr[x][y] = 0;
                else
                    picArr[x][y] = 1;
            }
        }

        return picArr;
    }

    private int[][] findAndMarkMinutiae(int[][] picArr, int[][] minuttiaeArr){
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                if (picArr[x][y] != 0)
                    minuttiaeArr[x][y] = calculatePixelMinutiaValue(x-1, y-1, picArr);
            }
        }

        return minuttiaeArr;
    }

    private int calculatePixelMinutiaValue(int startX, int startY, int[][] picArr){
        List<Integer> pixelList = new ArrayList<Integer>();

        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                pixelList.add(picArr[startX+x][startY+y]);
            }
        }

        pixelList.remove(4);

        int cn = (Math.abs((pixelList.get(4)-pixelList.get(2))) + Math.abs(pixelList.get(2)-pixelList.get(1)) +
                Math.abs(pixelList.get(1)-pixelList.get(0)) + Math.abs(pixelList.get(0)-pixelList.get(3)) +
                Math.abs(pixelList.get(3)-pixelList.get(5)) + Math.abs(pixelList.get(5)-pixelList.get(6)) +
                Math.abs(pixelList.get(6)-pixelList.get(7)) + Math.abs(pixelList.get(7)-pixelList.get(4)))/2;

        return (cn == 1 || cn == 3) ? cn : 255;
    }

    private void drawPictureFromPixels(PixelWriter writer, int[][] picArr){
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
               writer.setColor(x, y, (picArr[x][y] == 1) ? Color.BLACK : Color.WHITE);
            }
        }
    }

//    private void deleteFalseMinutiae(int[][] picArr, int[][] minutiaeArr){
//        int result = 0;
//        for (int y = 1; y < height - 1; y++) {
//            for (int x = 1; x < width - 1; x++) {
//                if (minutiaeArr[x][y] != 255){
//                    if (x > width/2){
//                        result = scanIfPixelShowInRightArea(picArr, x, y);
//
//                    }
//                    else{
//                        result = scanIfPixelShowInLeftArea(picArr, x, y);
//                    }
//
//                    if (result < 10)
//                        minutiaeArr[x][y] = 255;
//                }
//            }
//        }
//    }
//
//    private int scanIfPixelShowInLeftArea(int[][] picArr, int startX, int startY){
//        int stickCounter = 0;
//        int pointsToCheck = startX - 1;
//
//        for (int i = 0; i < pointsToCheck; i++) {
//            if (picArr[startX + i][startY] == 1 || picArr[startX + i][startY-1] == 1 || picArr[startX + i][startY+1] == 1)
//                ++stickCounter;
//        }
//
//        return stickCounter;
//    }
//
//     private int scanIfPixelShowInRightArea(int[][] picArr, int startX, int startY){
//        int stickCounter = 0;
//        int pointsToCheck = width - startX + 1;
//
//         for (int i = 0; i < pointsToCheck; i++) {
//             if (picArr[startX + i][startY] == 1 || picArr[startX + i][startY-1] == 1 || picArr[startX + i][startY+1] == 1)
//                 ++stickCounter;
//         }
//
//        return stickCounter;
//     }

//    private void deleteFalseMinutiae(int[][] picArr, int[][] minutiaeArr){
//        for (int y = 0; y < height; y++) {
//            for (int x = 0; x < width; x++) {
//                if (x > width/2 && minutiaeArr[x][y] != 255){
//                    if (checkLineRight(picArr, x, y))
//                        minutiaeArr[x][y] = 255;
//                }
//                else if (x < width/2 && minutiaeArr[x][y] != 255){
//                    if (checkLineLeft(picArr, x, y))
//                        minutiaeArr[x][y] = 255;
//                }
//            }
//        }
//    }
//
//    private boolean checkLineRight(int[][] picArr, int startX, int y){
//        int counter = 0;
//        for (int i = 0; i < width - 1- startX; i++) {
//            if (picArr[startX + i][y] == 1)
//                return false;
//        }
//
//        return true;
//    }
//
//    private boolean checkLineLeft(int[][] picArr, int startX, int y){
//        int counter = 0;
//        for (int i = 0; i < startX-1; i++) {
//            if (picArr[startX - i][y] == 1)
//                return false;
//        }
//
//        return true;
//    }

    private boolean isEdgePoint(int x, int y, int[][] picArr) {
        boolean leftSideConstraint = IntStream.rangeClosed(0, x - 1).mapToObj(v -> picArr[v][y]).filter(v -> v == 1).count() == 0;
        boolean rightSideConstraint = IntStream.rangeClosed(x + 1, width - 1).mapToObj(v -> picArr[v][y]).filter(v -> v == 1).count() == 0;
        boolean topSideConstraint = IntStream.rangeClosed(0, y - 1).mapToObj(v -> picArr[x][v]).filter(v -> v == 1).count() == 0;
        boolean bottomSideConstraint = IntStream.rangeClosed(y + 1, height - 1).mapToObj(v -> picArr[x][v]).filter(v -> v == 1).count() == 0;
        return leftSideConstraint || rightSideConstraint || topSideConstraint || bottomSideConstraint;
    }

    private void drawMinutiae(PixelWriter writer, int[][] minutiaeArr, int[][] picArr){
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (minutiaeArr[x][y] != 255){
                    switch (minutiaeArr[x][y]){
                        case 1:
                            if (!isEdgePoint(x, y, picArr))
                                drawPoint(writer, x-1, y-1, Color.RED);
                            break;

                        case 3:
                            drawPoint(writer, x-1, y-1, Color.BLUE);
                            break;
                    }
                }
            }
        }
    }

    private void drawPoint(PixelWriter writer, int startX, int startY, Color color){
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                writer.setColor(startX+x, startY+y, color);
            }
        }
    }

    private void neighborhoodBasedFalseMinutiaeClear(int[][] minutiaeArr) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (minutiaeArr[x][y] == 1 || minutiaeArr[x][y] == 3) {
                    for (int y2 = 0; y2 < height; y2++) {
                        for (int x2 = 0; x2 < width; x2++) {
                            if (x == x2 && y == y2)
                                continue;
                            if (minutiaeArr[x2][y2] != 1 && minutiaeArr[x2][y2] != 3)
                                continue;
                            if (minutiaeArr[x2][y2] != minutiaeArr[x][y])
                                continue;
                            final double DISTANCE_LIMIT = 20d;
                            double distance = Math.sqrt(Math.pow(x2 - x, 2) + Math.pow(y2 - y, 2));
                            if (distance <= DISTANCE_LIMIT)
                                minutiaeArr[x][y] = 255;
                        }
                    }
                }
            }
        }
    }

    public WritableImage startMinutiaeSearch(WritableImage image){
        PixelReader reader = image.getPixelReader();
        WritableImage newImage = new WritableImage(width, height);
        PixelWriter writer = newImage.getPixelWriter();
        int[][] minutiaeArr = new int[width][height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                minutiaeArr[x][y] = 255;
            }
        }

        int picArr[][] = convertToBinaryArr(reader);
        minutiaeArr = findAndMarkMinutiae(picArr, minutiaeArr);
        drawPictureFromPixels(writer, picArr);

        neighborhoodBasedFalseMinutiaeClear(minutiaeArr);

        drawMinutiae(writer, minutiaeArr, picArr);

        return newImage;
    }
}

