package sample;

import javafx.scene.chart.*;
import javafx.fxml.FXML;
import javafx.scene.image.WritableImage;


public class HistogramControler {

    private final WritableImage writableImage;
    private final Histogram histogram;

    @FXML
    private BarChart chart_red,
                    chart_green,
                    chart_blue,
                    chart_sumRGB;

    public int[] r, g, b, sumRGB;

    public HistogramControler(WritableImage writableImage, Histogram histogram){
        this.writableImage = writableImage;
        this.histogram = histogram;
    }

    public void initialize(){
        loadValues();
        printCharts();
    }

    public void loadValues(){
        this.r = histogram.r;
        this.g = histogram.g;
        this.b = histogram.b;
        this.sumRGB = histogram.sumRGB;
    }

    private void printCharts(){
        final CategoryAxis value = new CategoryAxis();
        final NumberAxis howMany = new NumberAxis();
        value.setLabel("Values");

        XYChart.Series redSeries = new XYChart.Series();
        XYChart.Series greenSeries = new XYChart.Series();
        XYChart.Series blueSeries = new XYChart.Series();
        XYChart.Series sumRGBSeries = new XYChart.Series();

        for (int i = 0; i < r.length; i++){
            redSeries.getData().add(new XYChart.Data(String.valueOf(i), r[i]));
        }
        for (int i = 0; i < g.length; i++){
            greenSeries.getData().add(new XYChart.Data(String.valueOf(i), g[i]));
        }
        for (int i = 0; i < b.length; i++){
            blueSeries.getData().add(new XYChart.Data(String.valueOf(i), b[i]));
        }
        for (int i = 0; i < sumRGB.length; i++){
            sumRGBSeries.getData().add(new XYChart.Data(String.valueOf(i), sumRGB[i]));
        }

        chart_red.getData().add(redSeries);
        chart_green.getData().add(greenSeries);
        chart_blue.getData().add(blueSeries);
        chart_sumRGB.getData().add(sumRGBSeries);
    }

}
