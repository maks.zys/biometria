package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class Histogram {

    public int[] r;
    public int[] g;
    public int[] b;
    public int[] sumRGB;

    private WritableImage writableImage,
                tmpWritableImage;

    Controller controller;

    public Histogram(WritableImage writableImage, Controller controller){
        this.writableImage = writableImage;
        this.controller = controller;
    }



    public void scanPictureAndCalculateRGB(){
        sumRGB = new int[256];
        r = new int[256];
        g = new int[256];
        b = new int[256];

        int red, green, blue, all;

        for(int i = 0; i < writableImage.getHeight(); i++){
            for (int j = 0; j < writableImage.getWidth(); j++){
                Color color = writableImage.getPixelReader().getColor(j, i);

                red = (int)(color.getRed() *255);
                green = (int)(color.getGreen() *255);
                blue = (int)(color.getBlue() *255);
                all = (int)((red + green + blue)/3);

                r[red]++;
                g[green]++;
                b[blue]++;
                sumRGB[all]++;
            }
        }
    }

    private double getAverage(double r, double g, double b) {
        return (r + g + b) / 3.0;
    }

    private double cutToFit(double value, double min, double max) {
        return Math.min(max, Math.max(min, value));
    }

    public void strechPictureAndCalculateRGB(Integer from, Integer to){

        scanPictureAndCalculateRGB();

        if (from == null) {
            from = 0;
            while (sumRGB[from] == 0 && from < 256)
                from++;
        }
        if (to == null) {
            to = 255;
            while (sumRGB[to] == 0 && to > -1)
                to--;
        }

        System.out.println("from = " + from);
        System.out.println("to = " + to);

        PixelReader pixelReader =  writableImage.getPixelReader();
        PixelWriter pixelWriter = writableImage.getPixelWriter();

        for (int y = 0; y < writableImage.getHeight(); y++)
            for (int x = 0; x < writableImage.getWidth(); x++) {
                Color oldColor = pixelReader.getColor(x, y);
                double oldColorValue = 255.0 * getAverage(oldColor.getRed(), oldColor.getGreen(), oldColor.getBlue());
                double newColorValue;
                if (oldColorValue < from)
                    newColorValue = 0.0;
                else if (oldColorValue > to)
                    newColorValue = 1.0;
                else
                    newColorValue = cutToFit((oldColorValue - from) / (to - from), 0.0, 1.0);
                pixelWriter.setColor(x, y, new Color(newColorValue, newColorValue, newColorValue, 1.0));
            }
    }

    public void equalizePictureAndCalculateRGB() {
        scanPictureAndCalculateRGB();

        double distributedSum = 0.0;
        double[] newValues = new double[256];
        for (int i = 0; i < 256; i++) {
            distributedSum += sumRGB[i];
            newValues[i] = 255.0 * distributedSum / (writableImage.getWidth() * writableImage.getHeight());
        }

        PixelReader pixelReader = writableImage.getPixelReader();
        PixelWriter pixelWriter = writableImage.getPixelWriter();

        for (int y = 0; y < writableImage.getHeight(); y++)
            for (int x = 0; x < writableImage.getWidth(); x++) {
                Color oldColor = pixelReader.getColor(x, y);
                int oldColorValue = (int) (255.0 * getAverage(oldColor.getRed(), oldColor.getGreen(), oldColor.getBlue()));
                double newColorValue = cutToFit(newValues[oldColorValue] / 255.0, 0.0, 1.0);
                pixelWriter.setColor(x, y, new Color(newColorValue, newColorValue, newColorValue, 1.0));
            }
    }

    public int[] updateTable(int[] table, int min, int max){
        int val;
        for (int i = 0; i < 256; i++){
            val = (256/(max - min)*(i - min));
            if (val > 255)
                table[i] = 255;
            else if (val < 0)
                table[i] = 0;
            else
                table[i] = val;
        }

        return table;
    }

    public void openHistogramWindow(int chooser) throws IOException {
        HistogramControler histogramControler = null;

        switch (chooser){
            case 1:
                scanPictureAndCalculateRGB();
                histogramControler = new HistogramControler(writableImage, this);
                break;

            case 2:
                int[] sliders = controller.getSliderValues();
                strechPictureAndCalculateRGB(sliders[0], sliders[1]);
                scanPictureAndCalculateRGB();
                histogramControler = new HistogramControler(writableImage, this);
                break;

            case 3:
                equalizePictureAndCalculateRGB();
                scanPictureAndCalculateRGB();
                histogramControler = new HistogramControler(writableImage, this);
                break;
        }

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("histograms.fxml"));
        fxmlLoader.setController(histogramControler);
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Histograms");
        stage.setScene(new Scene(root, 640, 420));
        stage.showAndWait();
    }

}
