package sample;

import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.image.*;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Filter {

    private int[] lowPassFilterArr = {1, 1, 1, 1, 1, 1, 1, 1, 1},
            prewittFilterArr = {-1, -1, -1, 0, 0, 0, 1, 1, 1},
            horizontalSobelFilterArr = {1, 2, 1, 0, 0, 0,-1, -2, -1},
            laplaceFilterArr = {0, -1, 0, -1, 4, -1, 0, -1, 0},
            cornersDetectionFilterArr = {-1, 1, 1, -1, -2, 1, -1, 1, 1};

    public int choosedWinSize;
    ComboBox winSize;

    public Filter(ComboBox winSize){
        choosedWinSize = 3;
        this.winSize = winSize;
    }

    private double calculatePixelValue(double[] pixels, int[] filterArr){
        double sum = 0, arrSum = 0;

        for (int i = 0; i < filterArr.length; i++)
            sum += pixels[i] * filterArr[i];

        for (double val : filterArr)
            arrSum += val;

        if (arrSum <= 0)
            return sum;

        return sum/arrSum;
    }

    private WritableImage filterPicture(WritableImage writableImage, int[] filterArr){
        PixelReader pixelReader = writableImage.getPixelReader();
        WritableImage newImage = new WritableImage((int)writableImage.getWidth(), (int)writableImage.getHeight());
        PixelWriter pixelWriter = newImage.getPixelWriter();

        checkChoosedWinSize();
        int halfMargin = choosedWinSize/2;

        if (choosedWinSize != 3){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("This filters have only 3x3 option refering to the task");
            alert.setContentText("Please change value in combobox");
            alert.showAndWait();
            return writableImage;
        }

        for (int y = 0; y < (int)writableImage.getHeight(); y++) {
            for (int x = 0; x < (int) writableImage.getWidth(); x++) {

                double[][] readedPixels;

                if ((((x - halfMargin) <= 0 || (y - halfMargin) <= 0)) ||
                        ((x + halfMargin) >= (int)writableImage.getWidth() || (y + halfMargin) >= (int)writableImage.getHeight())){
                    Color color = pixelReader.getColor(x, y);
                    pixelWriter.setColor(x, y, color);
                    continue;
                }

                readedPixels = readColors(pixelReader, x - halfMargin, y - halfMargin);

                double red = calculatePixelValue(readedPixels[0], filterArr);
                double green = calculatePixelValue(readedPixels[1], filterArr);
                double blue = calculatePixelValue(readedPixels[2], filterArr);

                if (red > 1 || red < 0)
                    red = (red > 1) ? 1 : 0;
                if (green > 1 || green < 0)
                    green = (green > 1) ? 1 : 0;
                if (blue > 1 || blue < 0)
                    blue = (blue > 1) ? 1 : 0;

                Color calculatedColor = new Color(red, green, blue, 1.0);

                pixelWriter.setColor(x, y, calculatedColor);
            }
        }

        return newImage;
    }

    public WritableImage lowPassFilter(WritableImage image){
        return filterPicture(image, lowPassFilterArr);
    }

    public WritableImage prewittFilter(WritableImage image){
        return filterPicture(image, prewittFilterArr);
    }

    public WritableImage horizontalSobelFilter(WritableImage image){
        return filterPicture(image, horizontalSobelFilterArr);
    }

    public WritableImage laplaceFilter(WritableImage image){
        return filterPicture(image, laplaceFilterArr);
    }

    public WritableImage cornersDetectionFilter(WritableImage image){
        return filterPicture(image, cornersDetectionFilterArr);
    }

    private double[][] readColors(PixelReader reader, int actualX, int actualY){
        double[][] colorsArray = new double[3][(int)Math.pow(choosedWinSize, 2)];
        int counter = 0;

        for (int y = 0; y < choosedWinSize; y++) {
            for (int x = 0; x < choosedWinSize; x++) {
                Color color = reader.getColor(actualX + x, actualY + y);
                colorsArray[0][counter] = color.getRed();
                colorsArray[1][counter] = color.getGreen();
                colorsArray[2][counter] = color.getBlue();

                ++counter;
            }
        }
        return colorsArray;
    }

    public void checkChoosedWinSize(){
        System.out.println(winSize.getValue().toString());
        choosedWinSize = (winSize.getValue().toString() == "3x3") ? 3 : 5;
    }

    public WritableImage medianFilter(WritableImage writableImage){
        PixelReader pixelReader = writableImage.getPixelReader();
        WritableImage newImage = new WritableImage((int)writableImage.getWidth(), (int)writableImage.getHeight());
        PixelWriter pixelWriter = newImage.getPixelWriter();

        checkChoosedWinSize();
        int halfMargin = choosedWinSize/2;

//        System.out.println("szerokość: " + writableImage.getWidth() + " Wysokość: " + writableImage.getHeight());

        for (int y = 1; y < (int)writableImage.getHeight() -1; y++) {
            for (int x = 1; x < (int) writableImage.getWidth() -1; x++) {

                double[][] readedPixels;

                if ((((x - halfMargin) <= 0 || (y - halfMargin) <= 0)) ||
                        ((x + halfMargin) >= (int)writableImage.getWidth() || (y + halfMargin) >= (int)writableImage.getHeight())){
                    Color color = pixelReader.getColor(x, y);
                    pixelWriter.setColor(x, y, color);
                    continue;
                }

                readedPixels = readColors(pixelReader, x - halfMargin, y - halfMargin);

                double red = findMedianInTable(readedPixels[0]);
                double green = findMedianInTable(readedPixels[1]);
                double blue = findMedianInTable(readedPixels[2]);

                if (red > 1 || red < 0)
                    red = (red > 1) ? 1 : 0;
                if (green > 1 || green < 0)
                    green = (green > 1) ? 1 : 0;
                if (blue > 1 || blue < 0)
                    blue = (blue > 1) ? 1 : 0;

                Color calculatedColor = new Color(red, green, blue, 1.0);

                pixelWriter.setColor(x, y, calculatedColor);
            }
        }

        return newImage;
    }

    private double findMedianInTable(double[] colorArr){
        colorArr = insertionSort(colorArr);
        return colorArr[(int)colorArr.length/2];
    }

    private double[] insertionSort(double[] colorArr){
        double value;
        int tmpHolder;

        for (int i=1; i<colorArr.length; i++){
            tmpHolder=i;
            value = colorArr[i];

            while (tmpHolder > 0 && colorArr[tmpHolder-1] > value){
                colorArr[tmpHolder] = colorArr[tmpHolder-1];
                tmpHolder--;
            }
            colorArr[tmpHolder]=value;
        }

        return colorArr;
    }

    public WritableImage kuwaharaFilter(WritableImage writableImage){
        PixelReader pixelReader = writableImage.getPixelReader();
        WritableImage newImage = new WritableImage((int)writableImage.getWidth(), (int)writableImage.getHeight());
        PixelWriter pixelWriter = newImage.getPixelWriter();

        int halfMargin = 2;
        choosedWinSize = 3;

        for (int y = 0; y < (int) writableImage.getHeight(); y++) {
            for (int x = 0; x < writableImage.getWidth(); x++) {

                if ((((x - halfMargin) <= 0 || (y - halfMargin) <= 0)) ||
                        ((x + halfMargin) >= (int)writableImage.getWidth() || (y + halfMargin) >= (int)writableImage.getHeight())){
                    Color color = pixelReader.getColor(x, y);
                    pixelWriter.setColor(x, y, color);
                    continue;
                }

                double[][] firstAreaPixels, secondAreaPixels, thirdAreaPixels, fourthAreaPixels;
                double[][] mean = new double[4][3];
                double[][] variance = new double[4][3];

                firstAreaPixels = readColors(pixelReader, x - 2, y -2);
                secondAreaPixels = readColors(pixelReader, x, y - 2);
                thirdAreaPixels = readColors(pixelReader, x - 2, y);
                fourthAreaPixels = readColors(pixelReader, x, y);

                mean[0] = calculateMean(firstAreaPixels);
                mean[1] = calculateMean(secondAreaPixels);
                mean[2] = calculateMean(thirdAreaPixels);
                mean[3] = calculateMean(fourthAreaPixels);

                variance[0] = calculateVariance(firstAreaPixels, mean[0]);
                variance[1] = calculateVariance(secondAreaPixels, mean[1]);
                variance[2] = calculateVariance(thirdAreaPixels, mean[2]);
                variance[3] = calculateVariance(fourthAreaPixels, mean[3]);

                int redIndex = findMin(0, variance),
                        greenIndex = findMin(1, variance),
                        blueIndex = findMin(2, variance);

                pixelWriter.setColor(x, y, new Color(mean[redIndex][0], mean[greenIndex][1], mean[blueIndex][2], 1.0));
            }
        }

        return newImage;
    }

    private double[] calculateMean(double[][] areaPixels){
        double[] mean = new double[3];
        double sumRed = 0, sumGreen = 0, sumBlue = 0;

        for (int i = 0; i < areaPixels[0].length; i++) {
            sumRed += areaPixels[0][i];
            sumGreen += areaPixels[1][i];
            sumBlue += areaPixels[2][i];
        }

        mean[0] = sumRed/9;
        mean[1] = sumGreen/9;
        mean[2] = sumBlue/9;

        return mean;
    }

    private double[] calculateVariance(double[][] areaPixels, double[] mean){
        double[] variance = new double[3];
        double sumRed = 0, sumGreen = 0, sumBlue = 0;

        for (int i = 0; i < areaPixels[0].length; i++) {
            sumRed += Math.pow((areaPixels[0][i] - mean[0]), 2);
            sumGreen += Math.pow((areaPixels[1][i] - mean[1]), 2);
            sumBlue += Math.pow((areaPixels[2][i] - mean[2]), 2);
        }

        variance[0] = sumRed/9;
        variance[1] = sumGreen/9;
        variance[2] = sumBlue/9;

        return variance;
    }

    private int findMin(int chanel, double[][] variances){
        double min = Integer.MAX_VALUE;
        int index = Integer.MAX_VALUE;
        for (int i = 0; i < 4; i++) {
            if (min > variances[i][chanel]){
                min = variances[i][chanel];
                index = i;
            }
        }
        return index;
    }
}
